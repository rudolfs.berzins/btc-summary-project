# BTC Summary Project

#### Purpose

Developed as part of a coding challenge.

Main tools used:
* Python
* [Pandas](https://pandas.pydata.org/)
* [Poetry](https://python-poetry.org/)
* [Pytest](https://docs.pytest.org/en/6.2.x/)
* [Docker](https://www.docker.com/)


## DESCRIPTION

`BTC Summary Project` allows users to generate summary statistics, subset file and price line-graph given a CSV file containing historical bitcoin price information.


## INSTALLATION

To run the program please ***pull*** the GIT repository on your local machine using

```bash
git clone https://gitlab.com/rudolfs.berzins/btc-summary-project.git
```

It is possible to choose from 3 installation options: Docker and Local Isolated (using Python virtual environments) and Local

### Docker - Recommended

**Prerequisite** - Docker and Docker CLI need to be present on the system. Follow instructions on their [website](https://docs.docker.com/get-docker/)

Execute the following command while in root directory of the project.

```bash
./run_in_docker.sh
```

This command will spin up a docker image that will contain `btc_summary` already installed. 
After that follow instructions described in Usage section. 
The shell script will also enter the docker container to allow usage of program in isolated environment. 
Also Docker will mount the current working directory as a Volume for the Docker image to be able to save `btc_summary` output.
To exit the container write `exit` or press CTRL(or CMD on Mac)+D.

### Local Isolated Install - Alternative

**Prerequisite** - Have Python 3.8+ present on the system with `python-venv` package installed. Starting from Python 3+ `python-venv` should be presinstalled on most systems.

Execute the following command while in root directory of the project.
```bash
source run_locally.sh
```

This command will create a Python virtual environment and will install `btc_summary` using `poetry`.
After that follow instructions described in Usage section.
It will also activate the virtual environment creating an isloated environment where `btc_summary` can be used.
To exit this virtual environment, write `deactivate` in command line.

### Local Install - Not Recommended

**Prerequisite** - Have Python 3.8+ present on the system with `poetry` installed globally.

Execute the following command while in root directory of the project.
```bash
poetry install
```

This command will install `btc_summary` and it's dependencies in your system directly.
It might have unintended consequences and it is generaly not recommended. 


## USAGE

After installation `btc_summary` is ready for use. 

To see help write

```bash
btc_summary -h
```

**Help**

```bash
usage: btc_summary [-h] -i INPUT_FILE_PATH [-d INITIAL_DATE] [-o OUTPUT_FOLDER] [-s SUFFIX]

Given a bitcoin price history file in CSV format, generate summary statistics, summary file and price line graph ending with a given
date looking a year back. Default date - last available date in bitcoin price history file

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE_PATH, --input_file_path INPUT_FILE_PATH
                        Path to input CSV file
  -d INITIAL_DATE, --initial_date INITIAL_DATE
                        Date from which to calculate statistics. Has to be in ISO 8601 (YYYY-MM-DD) format. Default - last available
                        date from input_file_path
  -o OUTPUT_FOLDER, --output_folder OUTPUT_FOLDER
                        Path to folder where year summary and line graph will be saved. If folder does not exist it will be created.
                        Default - current working directory.
  -s SUFFIX, --suffix SUFFIX
                        A suffix that will be added to output file paths to help differentiate them. Example - in:'TEST_SUFFIX'
                        out:'/path/to/file_TEST_SUFFIX.ext Default - date range string in format 'YYYY-MM-DD_YYYY-MM-DD'
```

***Example usage***

* Using file `btc.csv` located in folder `data/` generate statistics for the past 365 days from the last data point's date in the csv file provided. Save output in the current working directory.

```bash
btc_summary -i data/btc.csv
```

* Using file `btc.csv` located in folder `data/` generate statistics for the year ending with 2017-01-01. Save output in folder named `summary_output` and substitute suffix with the word `2016_overview`

```bash
btc_summary -i data/btc.csv -d 2017-01-01 -o summary_output -s 2016_overview
```

## TESTING

To run all tests, after installation execute the following command in the root project directory

```bash
pytest tests
```

*Note* - it might be necessary to write `source btc_summary_venv/bin/activate` if using **Local Isolated Install** and it throws an error mentioning that pytest is not installed.

There are a number of levels for tests marked so it is possible to execute *unit*, *integration* and *validation* tests seperately from each other using the following commands

```bash
pytest tests -m unit
pytest tests -m integration
pytest tests -m validation
```

### Credits

Developed by [Rudolfs Berzins](https://www.linkedin.com/in/rudolfsberzinsbinf/)

All inquiries about code please send via [email](rudolfs.berzins.bio@gmail.com)

#### P.S.

I know it's bad practice to include `.env` files in the code repository. That is not an oversight on my part, I wanted to ease user access to not ask them to create `.env` files themselves. In Production scenarios this file would probably be part of K8s Secrets, AWS Secrets Manager or similar service.









