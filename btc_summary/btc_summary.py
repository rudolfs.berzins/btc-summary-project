import argparse
import os
import sys

from datetime import datetime
from dateutil.relativedelta import relativedelta
from pathlib import Path

import pandas as pd
from dotenv import load_dotenv

from btc_summary.utils import (
    find_closest_date,
    ensure_path,
    get_date_strings,
    check_initial_date_range,
    check_initial_date_presence,
    generate_summary_file_paths,
)

load_dotenv()
pd.options.display.float_format = "{:.2f}".format

EUR_CONVERSION_RATE = float(os.getenv("EUR_CONVERSION_RATE"))


def args_parser():

    parser = argparse.ArgumentParser(
        description="Given a bitcoin price history file in CSV format, generate summary statistics, summary file and price line graph ending with a given date looking a year back. Default date - last available date in bitcoin price history file"
    )

    parser.add_argument(
        "-i", "--input_file_path", help="Path to input CSV file", required=True
    )
    parser.add_argument(
        "-d",
        "--initial_date",
        help="Date from which to calculate statistics. Has to be in ISO 8601 (YYYY-MM-DD) format. Default - last available date from input_file_path",
        type=datetime.fromisoformat,
    )
    parser.add_argument(
        "-o",
        "--output_folder",
        help="Path to folder where year summary and line graph will be saved. If folder does not exist it will be created. Default - current working directory.",
        default=Path.cwd(),
    )
    parser.add_argument(
        "-s",
        "--suffix",
        help="A suffix that will be added to output file paths to help differentiate them. Example - in:'TEST_SUFFIX' out:'/path/to/file_TEST_SUFFIX.ext Default - date range string in format 'YYYY-MM-DD_YYYY-MM-DD'",
    )

    return parser


def read_data_into_dataframe(file_path):

    return pd.read_csv(
        file_path,
        header=0,
        parse_dates=["date"],
        index_col=["date"],
        usecols=[
            "date",
            "marketcap(USD)",
            "price(USD)",
            "generatedCoins",
            "paymentCount",
        ],
    )


def extract_year_data(dataframe, initial_date):

    # Add one day so it does not take into account the last day (i.e. sums to 365 days)
    approximate_date = initial_date - relativedelta(years=1) + relativedelta(days=1)
    closest_available_date = find_closest_date(dataframe.index, approximate_date)
    year_dataframe = dataframe.loc[closest_available_date:initial_date]

    return year_dataframe


def convert_to_euro(year_dataframe, conversion_rate=EUR_CONVERSION_RATE):

    conversion_columns = ["marketcap", "price"]

    converted_dataframe = year_dataframe.copy()

    for conv_col in conversion_columns:
        converted_dataframe[f"{conv_col}(EUR)"] = (
            converted_dataframe.loc[:, (f"{conv_col}(USD)")] * conversion_rate
        )

    converted_dataframe = converted_dataframe.drop(
        columns=[f"{conv_col}(USD)" for conv_col in conversion_columns], axis=1
    )

    return converted_dataframe


def save_dataframe(converted_dataframe, output_file_path):

    ensure_path(output_file_path)
    converted_dataframe.to_csv(output_file_path)
    print(
        f"Clean subset dataframe saved in {os.path.abspath(output_file_path)}\n",
        file=sys.stderr,
    )

    return True


def print_summary(converted_dataframe):

    min_date_string, max_date_string = get_date_strings(converted_dataframe)
    total_counts_generated = round(sum(converted_dataframe.generatedCoins), 2)

    describe_dataframe = converted_dataframe.describe()
    describe_dataframe["marketcap(EUR)"] = (
        describe_dataframe.loc[:, ("marketcap(EUR)")] / 1e9
    )
    describe_dataframe = describe_dataframe.T[["min", "max", "mean"]]
    describe_dataframe = describe_dataframe.rename(columns={"mean": "average"}).reindex(
        ["marketcap(EUR)", "price(EUR)", "generatedCoins", "paymentCount"]
    )

    summary_text = f"""\nTotal amount of generated coins between {min_date_string} and {max_date_string}\n{total_counts_generated}\nSummary:\n{describe_dataframe}\n\n"""
    print(summary_text)

    return True


def save_plot_years_price(converted_dataframe, plot_path):

    ensure_path(plot_path)

    min_date_string, max_date_string = get_date_strings(converted_dataframe)

    line_graph = converted_dataframe.plot.line(y="price(EUR)")

    fig = line_graph.get_figure()
    ax = line_graph.axes
    fig.set_size_inches(18, 15)
    ax.set_title(
        f"Bitcoin price ({min_date_string} - {max_date_string}) in EUR", fontsize=24
    )
    ax.set_xlabel("Date", fontsize=24)
    ax.set_ylabel("Price in EUR", fontsize=24)
    ax.tick_params(axis="both", which="minor", labelsize=20)
    ax.tick_params(axis="both", which="major", labelsize=24)
    ax.legend(prop={"size": 20})
    ax.set_axisbelow(True)
    ax.yaxis.grid(color="gray", linestyle="dashed")
    ax.xaxis.grid(color="gray", linestyle="dashed")

    fig.savefig(
        plot_path, transparent=False, facecolor="w", edgecolor="w", bbox_inches="tight"
    )
    print(
        f"\nPrice plot saved in {os.path.abspath(plot_path)}",
        file=sys.stderr,
    )

    return True


def create_btc_summary(args=None):

    if args is None:
        args = args_parser().parse_args()

    file_path = args.input_file_path
    initial_date = args.initial_date
    output_folder = args.output_folder
    suffix = args.suffix

    dataframe = read_data_into_dataframe(file_path)

    if initial_date is None:
        initial_date = dataframe.index.max()
    else:
        check_initial_date_range(dataframe, initial_date)
        initial_date = check_initial_date_presence(dataframe, initial_date)

    year_dataframe = extract_year_data(dataframe, initial_date)
    converted_dataframe = convert_to_euro(year_dataframe)

    if suffix is None:
        min_date_string, max_date_string = get_date_strings(converted_dataframe)
        suffix = f"{min_date_string}_{max_date_string}"

    summary_path, plot_path = generate_summary_file_paths(output_folder, suffix)

    save_dataframe(converted_dataframe, summary_path)
    print_summary(converted_dataframe)
    save_plot_years_price(converted_dataframe, plot_path)


if __name__ == "__main__":
    create_btc_summary()
