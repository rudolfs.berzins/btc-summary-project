import os

from calendar import month_abbr
from pathlib import Path

from dateutil.relativedelta import relativedelta

from btc_summary.exceptions import OutOfRangeException


def find_closest_date(dates, target_date):
    return min(dates, key=lambda x: abs(x - target_date))


def ensure_path(output_path):
    # Ensure that full output_file_path exists
    # if not, create it
    head, _ = os.path.split(output_path)
    Path(head).mkdir(parents=True, exist_ok=True)
    return True


def get_date_strings(converted_dataframe):
    min_date, max_date = (
        converted_dataframe.index.min(),
        converted_dataframe.index.max(),
    )
    min_date_string = f"{min_date.year}-{month_abbr[min_date.month]}-{min_date.day}"
    max_date_string = f"{max_date.year}-{month_abbr[max_date.month]}-{max_date.day}"

    return (min_date_string, max_date_string)


def check_initial_date_range(dataframe, initial_date):
    min_date, max_date = dataframe.index.min(), dataframe.index.max()
    if not (min_date + relativedelta(years=1)) <= initial_date <= max_date:
        min_available_date_string = (min_date + relativedelta(years=1)).strftime(
            "%Y-%m-%d"
        )
        max_available_date_string = max_date.strftime("%Y-%m-%d")
        raise OutOfRangeException(
            f"initial_date must be between {min_available_date_string} and {max_available_date_string}"
        )
    return True


def check_initial_date_presence(dataframe, initial_date):
    # Check if initial_date is present in dataframe else, find closest date.
    if initial_date not in dataframe.index:
        print(f"WARN: Date {initial_date} is not present in dataframe")
        initial_date = find_closest_date(dataframe.index, initial_date)
        print(f"WARN: Using {initial_date} instead")

    return initial_date


def generate_summary_file_paths(output_folder, suffix):

    summary_base = f"BTC_data_subset_{suffix}.csv"
    plot_base = f"BTC_line_graph_{suffix}.png"

    return (
        os.path.join(output_folder, file_base)
        for file_base in [summary_base, plot_base]
    )
