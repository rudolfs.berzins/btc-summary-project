#!/bin/sh

# Create a virtual environment and activate it
python3 -m venv btc_summary_venv
source btc_summary_venv/bin/activate

# Install poetry
pip install poetry

# Install package
poetry install