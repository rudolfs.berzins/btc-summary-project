#-------------------------- DEPENDENCIES ------------------------#
FROM python:3.8-slim

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VERSION=1.0.0

RUN pip install poetry

WORKDIR /btc_summary
COPY poetry.lock pyproject.toml /btc_summary/

RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-ansi

COPY . /btc_summary



