import os
from datetime import datetime
import pytest
import pandas as pd


@pytest.fixture()
def mini_converted_df():
    data = {
        "date": [
            datetime.fromisoformat("2020-01-01"),
            datetime.fromisoformat("2020-01-02"),
        ],
        "generatedCoins": [1000, 2000],
        "paymentCount": [10, 20],
        "marketcap(EUR)": [3000, 4000],
        "price(EUR)": [100, 200],
    }
    mini_df = pd.DataFrame(data).set_index("date")
    yield mini_df


@pytest.fixture()
def converted_df():
    data = {
        "date": [
            datetime.fromisoformat("2020-01-01"),
            datetime.fromisoformat("2020-01-02"),
            datetime.fromisoformat("2021-02-03"),
            datetime.fromisoformat("2021-02-04"),
            datetime.fromisoformat("2021-03-05"),
            datetime.fromisoformat("2021-03-06"),
        ],
        "generatedCoins": [1000, 2000, 3000, 4000, 5000, 6000],
        "paymentCount": [10, 20, 30, 40, 50, 60],
        "marketcap(EUR)": [3000, 4000, 5000, 6000, 7000, 8000],
        "price(EUR)": [100, 200, 300, 400, 500, 600],
    }

    df = pd.DataFrame(data).set_index("date")
    yield df


@pytest.fixture()
def base_df():
    data = {
        "date": [
            datetime.fromisoformat("2020-01-01"),
            datetime.fromisoformat("2020-01-02"),
            datetime.fromisoformat("2021-02-03"),
            datetime.fromisoformat("2021-02-04"),
            datetime.fromisoformat("2021-03-05"),
            datetime.fromisoformat("2021-03-06"),
        ],
        "generatedCoins": [1000, 2000, 3000, 4000, 5000, 6000],
        "paymentCount": [10, 20, 30, 40, 50, 60],
        "marketcap(USD)": [3000, 4000, 5000, 6000, 7000, 8000],
        "price(USD)": [100, 200, 300, 400, 500, 600],
    }

    df = pd.DataFrame(data).set_index("date")
    yield df


@pytest.fixture()
def test_btc_file(base_df):

    file_path = "test_btc_file.csv"
    base_df.to_csv(file_path)
    yield file_path
    os.remove(file_path)
