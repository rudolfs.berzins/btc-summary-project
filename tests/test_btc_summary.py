import os
import shutil
import sys
from datetime import datetime

import pytest
import pandas as pd

from btc_summary import __version__
from btc_summary.btc_summary import (
    args_parser,
    extract_year_data,
    convert_to_euro,
    save_dataframe,
    print_summary,
    save_plot_years_price,
    create_btc_summary,
)


@pytest.mark.integration
def test_extract_year_data_correctly_extracts_approximate_year(base_df):
    test_initial_date = datetime.fromisoformat("2021-02-04")
    expected_data = {
        "date": [
            datetime.fromisoformat("2020-01-02"),
            datetime.fromisoformat("2021-02-03"),
            datetime.fromisoformat("2021-02-04"),
        ],
        "generatedCoins": [2000, 3000, 4000],
        "paymentCount": [20, 30, 40],
        "marketcap(USD)": [4000, 5000, 6000],
        "price(USD)": [200, 300, 400],
    }

    expected_df = pd.DataFrame(expected_data).set_index("date")

    test_df = extract_year_data(base_df, test_initial_date)

    assert (test_df.values == expected_df.values).all()


@pytest.mark.integration
def test_convert_to_euro_correctly_converts_columns(base_df):

    mock_conversion_rate = 0.5

    data = {
        "date": [
            datetime.fromisoformat("2020-01-01"),
            datetime.fromisoformat("2020-01-02"),
            datetime.fromisoformat("2021-02-03"),
            datetime.fromisoformat("2021-02-04"),
            datetime.fromisoformat("2021-03-05"),
            datetime.fromisoformat("2021-03-06"),
        ],
        "generatedCoins": [1000, 2000, 3000, 4000, 5000, 6000],
        "paymentCount": [10, 20, 30, 40, 50, 60],
        "marketcap(EUR)": [
            x * mock_conversion_rate for x in [3000, 4000, 5000, 6000, 7000, 8000]
        ],
        "price(EUR)": [
            x * mock_conversion_rate for x in [100, 200, 300, 400, 500, 600]
        ],
    }

    expected_df = pd.DataFrame(data).set_index("date")

    test_df = convert_to_euro(base_df, conversion_rate=mock_conversion_rate)

    assert (test_df.values == expected_df.values).all()


@pytest.mark.integration
def test_save_dataframe_works_correctly(converted_df):

    test_output_file_path = os.path.join("dataframe", "folder", "with", "dataframe")

    expected_columns = [
        "date",
        "generatedCoins",
        "paymentCount",
        "marketcap(EUR)",
        "price(EUR)",
    ]

    assert not os.path.exists(test_output_file_path)
    save_dataframe(converted_df, test_output_file_path)
    assert os.path.exists(test_output_file_path)

    with open(test_output_file_path) as tofp:
        header = tofp.readline()

    header_columns = header.strip().split(",")

    assert set(expected_columns) == set(header_columns)

    shutil.rmtree("dataframe")


@pytest.mark.integration
def test_print_summary_works_correctly(converted_df, capsys):

    print_summary(converted_df)
    out, _ = capsys.readouterr()
    assert out  # It's not as important what the content is, but that it prints out


@pytest.mark.integration
def test_save_plot_years_price_works_correctly(converted_df):

    test_output_file_path = os.path.join("plot", "folder", "with", "plot.png")

    assert not os.path.exists(test_output_file_path)
    save_plot_years_price(converted_df, test_output_file_path)
    assert os.path.exists(
        test_output_file_path
    )  # Again not so improtant about what is on the plot, but that the plot exists

    shutil.rmtree("plot")


@pytest.mark.validation
def test_create_btc_summary_works_correctly(test_btc_file, capsys):

    initial_date = "2021-02-04"
    output_folder = os.path.join("testing", "output", "folder")
    suffix = "test"

    args_string = f"-i {test_btc_file} -d {initial_date} -o {output_folder} -s {suffix}"

    args = args_parser().parse_args(args_string.split())

    create_btc_summary(args)

    # Assert that summary was printed
    summary_out, _ = capsys.readouterr()
    assert summary_out

    # Assert that output_folder was created
    assert os.path.isdir(output_folder)

    # Assert that output_folder contains 2 files with suffix in
    # their file names
    folder_content = os.listdir(output_folder)
    assert len(folder_content) == 2
    for _file in folder_content:
        assert suffix in _file

    shutil.rmtree("testing")
