import os
import shutil
from datetime import datetime

import pytest

from btc_summary.utils import (
    check_initial_date_presence,
    find_closest_date,
    ensure_path,
    get_date_strings,
    check_initial_date_range,
    generate_summary_file_paths,
)

from btc_summary.exceptions import OutOfRangeException


@pytest.mark.unit
@pytest.mark.parametrize(
    "input_inital_date, expected_date",
    [
        (datetime.fromisoformat("2020-02-03"), datetime.fromisoformat("2020-02-03")),
        (datetime.fromisoformat("2020-01-31"), datetime.fromisoformat("2020-02-01")),
        (datetime.fromisoformat("2020-02-05"), datetime.fromisoformat("2020-02-06")),
    ],
)
def test_find_closest_date_correctly_finds_date(input_inital_date, expected_date):

    input_date_array = [
        datetime.fromisoformat("2020-02-01"),
        datetime.fromisoformat("2020-02-03"),
        datetime.fromisoformat("2020-02-06"),
    ]

    assert find_closest_date(input_date_array, input_inital_date) == expected_date


@pytest.mark.unit
def test_ensure_path_creates_necessary_folders():

    folder_path = os.path.join("some", "folder", "path")

    assert not os.path.exists(folder_path)
    ensure_path(folder_path)
    assert os.path.exists(os.path.dirname(folder_path))
    shutil.rmtree("some")


@pytest.mark.unit
def test_get_date_strings_returns_correct_strings(mini_converted_df):

    test_date_string = get_date_strings(mini_converted_df)
    expected_date_strings = ("2020-Jan-1", "2020-Jan-2")

    assert test_date_string == expected_date_strings


@pytest.mark.unit
def test_check_initial_date_range_allows_correct_initial_date(converted_df):

    test_initial_date = datetime.fromisoformat("2021-01-04")

    assert check_initial_date_range(converted_df, test_initial_date)


@pytest.mark.unit
@pytest.mark.parametrize(
    "test_initial_date",
    [
        datetime.fromisoformat("2021-12-12"),  # Too large
        datetime.fromisoformat("1999-12-12"),  # Too small
        datetime.fromisoformat("2020-02-02"),  # Not enough data for a year back
    ],
)
def test_check_initial_date_range_raises_exception_with_bad_dates(
    converted_df, test_initial_date
):

    with pytest.raises(OutOfRangeException):
        check_initial_date_range(converted_df, test_initial_date)


@pytest.mark.unit
@pytest.mark.parametrize(
    "test_initial_date, expected_result",
    [
        (
            datetime.fromisoformat("2020-01-01"),
            datetime.fromisoformat("2020-01-01"),
        ),  # Present date
        (
            datetime.fromisoformat("2020-01-03"),
            datetime.fromisoformat("2020-01-02"),
        ),  # Missing date
    ],
)
def test_check_initial_date_presence_correctly_checks_presence(
    mini_converted_df, test_initial_date, expected_result
):

    result_initial_date = check_initial_date_presence(
        mini_converted_df, test_initial_date
    )

    assert result_initial_date == expected_result


@pytest.mark.unit
def test_generate_summary_file_paths_generates_correct_file_paths():
    test_output_folder = os.path.join("some", "folder")
    test_suffix = "test_suffix"

    fps = generate_summary_file_paths(test_output_folder, test_suffix)

    # Generic test because file names can change, but we need to
    # add suffix and output folder nonetheless
    for filepath in fps:
        assert test_output_folder in filepath
        assert test_suffix in filepath
